/* eslint-env mocha */
'use strict'

process.env.NODE_ENV = 'test'

const chai = require('chai')
// const { expect } = require('chai')
chai.use(require('chai-fs'))
chai.use(require('chai-cheerio'))
chai.use(require('chai-spies'))
// dirty-chai must be loaded after the other plugins
// see https://github.com/prodatakey/dirty-chai#plugin-assertions
chai.use(require('dirty-chai'))
const fs = require('fs')
// const fsextra = require('fs-extra')
const path = require('path')
const http = require('http')
const https = require('https')
// QUESTION should the cache version track the major version of Antora?
const CONTENT_CACHE_FOLDER = 'content/2'
const getCacheDir = require('cache-directory')
const ContentCatalog = require('@antora/content-classifier/lib/content-catalog')
const { lookup: resolveMimeType } = require('./mime-types-with-asciidoc')
const { deferExceptions, expect, removeSyncForce } = require('./test-utils')

const RemoteFetch = require('../lib/index')

const CACHE_DIR = getCacheDir('antora-test')
const PROJECT_DIR = 'remote-fetch'

const DEFAULT_RESOURCE_ID = {
  version: '1.0',
  component: 'c1',
  module: 'm1',
  dirname: 't1/',
}

function clean () {
  removeSyncForce(CACHE_DIR)
}

// returns:
// false if only the cache dir is missing
// 'no-cache-dir' if a parent directory is also missing
// array of (dir) names (hashes) in the cache directory otherwise.
async function getCachedFileNames () {
  const cache = path.join(CACHE_DIR, CONTENT_CACHE_FOLDER, PROJECT_DIR)
  let stat
  try {
    stat = await fs.promises.lstat(cache)
    if (!stat) {
      return false
    }
  } catch (e) {
    if (e.errno === -2) {
      return 'no-cache-dir'
    } else {
      throw e
    }
  }
  if ((!stat.isDirectory())) {
    throw new Error(`cacheDir ${cache} is not a directory`)
  }
  return await fs.promises.readdir(cache)
}

const DEFAULT_CACHE_INFO = {
  preferredCacheDir: CACHE_DIR,
  projectDir: PROJECT_DIR,
  startDir: '.',
}

describe('https remote-fetch tests', () => {
  var httpServer
  var httpsServer
  var httpRequests
  var contentCatalog
  var remoteFetch

  before(async () => {
    const options = {
      key: fs.readFileSync('test/fixtures/keys/server.key'),
      cert: fs.readFileSync('test/fixtures/keys/server.cert'),
    }

    const REDIRECT_RX = /^redirect-(\d{3})\/(http|https)\/(.*)$/
    const requestListener = (req, resp) => {
      const url = req.url.slice(1)
      httpRequests.push(url)
      const match = url.match(REDIRECT_RX)
      if (match) {
        const code = match[1]
        const protocol = match[2]
        const to = match[3]
        resp.writeHead(code, { location: `${protocol}://${req.headers.host}/${to}` })
        resp.end()
      } else {
        try {
          const file = fs.readFileSync(`test/fixtures/${url}`)
          const type = resolveMimeType(url.extname)
          resp.writeHead(200, `{content-type: ${type}}`)
          resp.write(file)
          resp.end()
        } catch (e) {
          resp.writeHead(404, 'file not found')
          resp.end()
        }
      }
    }
    httpServer = http.createServer(options, requestListener).listen({ port: 8081, hostname: 'localhost' })
    httpsServer = https.createServer(options, requestListener).listen({ port: 8082, hostname: 'localhost' })
  })

  after(async () => {
    httpServer.close()
    httpsServer.close()
  })

  beforeEach(() => {
    contentCatalog = new ContentCatalog()
    clean()
    httpRequests = []
  })

  afterEach(() => {
    clean()
  })

  async function setup (config) {
    if (config.rejectUnauthorized === false) {
      process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
    } else {
      delete process.env.NODE_TLS_REJECT_UNAUTHORIZED
    }
    remoteFetch = new RemoteFetch(config)
    await remoteFetch.initialize()
  }

  ;[
    {
      protocol: 'http://localhost:8081',
      noCacheConfig: { allowHttp: true },
      requestCount: 1,
      rejectMessage: 'Http protocol not allowed for ',
      missingMessage: /Unexpected statusCode 404 for http:\/\/localhost:8081[^ ]*/,
    },
    {
      protocol: 'http://localhost:8081/redirect-301/http',
      noCacheConfig: { allowHttp: true },
      requestCount: 2,
      rejectMessage: 'Http protocol not allowed for ',
      missingMessage: /Unexpected statusCode 404 for http:\/\/localhost:8081[^ ]*/,
    },
    {
      protocol: 'http://localhost:8081/redirect-302/http',
      noCacheConfig: { allowHttp: true },
      requestCount: 2,
      rejectMessage: 'Http protocol not allowed for ',
      missingMessage: /Unexpected statusCode 404 for http:\/\/localhost:8081[^ ]*/,
    },
    {
      protocol: 'http://localhost:8081/redirect-307/http',
      noCacheConfig: { allowHttp: true },
      requestCount: 2,
      rejectMessage: 'Http protocol not allowed for ',
      missingMessage: /Unexpected statusCode 404 for http:\/\/localhost:8081[^ ]*/,
    },
    {
      protocol: 'https://localhost:8082',
      noCacheConfig: { rejectUnauthorized: false },
      requestCount: 1,
      rejectMessage: 'https error: Error: certificate has expired for url:',
      missingMessage: /Unexpected statusCode 404 for https:\/\/localhost:8082[^ ]*/,
    },
    {
      protocol: 'https://localhost:8082/redirect-301/https',
      noCacheConfig: { rejectUnauthorized: false },
      requestCount: 2,
      rejectMessage: 'https error: Error: certificate has expired for url:',
      missingMessage: /Unexpected statusCode 404 for https:\/\/localhost:8082[^ ]*/,
    },
    {
      protocol: 'https://localhost:8082/redirect-302/https',
      noCacheConfig: { rejectUnauthorized: false },
      requestCount: 2,
      rejectMessage: 'https error: Error: certificate has expired for url:',
      missingMessage: /Unexpected statusCode 404 for https:\/\/localhost:8082[^ ]*/,
    },
    {
      protocol: 'https://localhost:8082/redirect-307/https',
      noCacheConfig: { rejectUnauthorized: false },
      requestCount: 2,
      rejectMessage: 'https error: Error: certificate has expired for url:',
      missingMessage: /Unexpected statusCode 404 for https:\/\/localhost:8082[^ ]*/,
    },
    {
      protocol: `file://${__dirname}/fixtures`,
      noCacheConfig: { allowedFilePaths: [`${__dirname}`] },
      requestCount: 0,
      rejectMessage: /Path [^ ]* not allowed/,
      missingMessage: /Could not read local file file:\/\/\/[^ ]*/,
    },
    {
      protocol: `file://${__dirname}/fixtures`,
      noCacheConfig: { allowedFilePaths: [RemoteFetch.$allPaths] },
      requestCount: 0,
      rejectMessage: /Path [^ ]* not allowed/,
      missingMessage: /Could not read local file file:\/\/\/[^ ]*/,
    },
  ].forEach(({ protocol, noCacheConfig, requestCount, rejectMessage, missingMessage }) => {
    const cacheConfig = Object.assign({ cacheInfo: DEFAULT_CACHE_INFO }, noCacheConfig)
    describe(`tests with protocol ${protocol.slice(0, 5)}`, () => {
      ;[
        ['patha/to/a.adoc', 'page', 'utf8'],
        ['examples/fire.jpeg', 'image', 'binary'],
        ['examples/stem.svg', 'image', 'binary'],
        ['examples/sample.pdf', 'attachment', 'binary'],
      ].forEach(([resourcePath, family, encoding]) => {
        describe(`tests with ${resourcePath}`, () => {
          const basename = path.basename(resourcePath)
          it(`should fetch ${protocol.slice(0, 5)} `, async () => {
            expect(await getCachedFileNames()).to.equal('no-cache-dir')
            await setup(cacheConfig)
            const key = remoteFetch.fetchToContentCatalog(`${protocol}/${resourcePath}`, contentCatalog, DEFAULT_RESOURCE_ID, family, encoding)
            await remoteFetch.complete()
            expect(key).to.equal(`1.0@c1:m1:${family}$t1/${basename}`)
            const files = contentCatalog.getAll()
            expect(files.length).to.equal(1)
            const cachedFilenames = await getCachedFileNames()
            expect(cachedFilenames.length).to.equal(1)
            expect(httpRequests.length).to.equal(requestCount)
            expect(fs.readFileSync(path.join(__dirname, 'fixtures', resourcePath)).compare(files[0].contents)).to.equal(0)
            expect(fs.readFileSync(path.join(CACHE_DIR, CONTENT_CACHE_FOLDER, PROJECT_DIR, cachedFilenames[0], 'contents')).compare(files[0].contents)).to.equal(0)
          })

          it(`should not fetch ${protocol.slice(0, 5)} without config`, async () => {
            expect(await getCachedFileNames()).to.equal('no-cache-dir')
            await setup({ cacheInfo: DEFAULT_CACHE_INFO })
            const key = remoteFetch.fetchToContentCatalog(`${protocol}/${resourcePath}`, contentCatalog, DEFAULT_RESOURCE_ID, family, encoding)
            expect(key).to.equal(`1.0@c1:m1:${family}$t1/${basename}`)
            const d = await deferExceptions(() => Promise.all(remoteFetch.promises))
            expect(d).to.throw(rejectMessage)
            const files = contentCatalog.getAll()
            expect(files.length).to.equal(0)
            expect((await getCachedFileNames()).length).to.equal(0)
            expect(httpRequests.length).to.equal(0)
          })

          it(`should fetch ${protocol.slice(0, 5)} from cache`, async () => {
            expect(await getCachedFileNames()).to.equal('no-cache-dir')
            await setup(cacheConfig)
            const key = remoteFetch.fetchToContentCatalog(`${protocol}/${resourcePath}`, contentCatalog, DEFAULT_RESOURCE_ID, family, encoding)
            await remoteFetch.complete()
            expect(key).to.equal(`1.0@c1:m1:${family}$t1/${basename}`)
            const files = contentCatalog.getAll()
            expect(files.length).to.equal(1)
            expect((await getCachedFileNames()).length).to.equal(1)
            expect(httpRequests.length).to.equal(requestCount)

            await setup({ cacheInfo: DEFAULT_CACHE_INFO })
            const key2 = remoteFetch.fetchToContentCatalog(`${protocol}/${resourcePath}`, contentCatalog, Object.assign({}, DEFAULT_RESOURCE_ID, { module: 'm2' }), family, encoding)
            await remoteFetch.complete()
            expect(key2).to.equal(`1.0@c1:m2:${family}$t1/${basename}`)
            const files2 = contentCatalog.getAll()
            expect(files2.length).to.equal(2)
            expect((await getCachedFileNames()).length).to.equal(1)
            expect(httpRequests.length).to.equal(requestCount)
          })

          it(`should use ${protocol.slice(0, 5)} from memory cache`, async () => {
            expect(await getCachedFileNames()).to.equal('no-cache-dir')
            await setup(cacheConfig)
            const key = remoteFetch.fetchToContentCatalog(`${protocol}/${resourcePath}`, contentCatalog, DEFAULT_RESOURCE_ID, family, encoding)
            await remoteFetch.complete()
            expect(key).to.equal(`1.0@c1:m1:${family}$t1/${basename}`)
            const files = contentCatalog.getAll()
            expect(files.length).to.equal(1)
            expect((await getCachedFileNames()).length).to.equal(1)
            expect(httpRequests.length).to.equal(requestCount)

            //empty disk cache
            clean()
            const key2 = remoteFetch.fetchToContentCatalog(`${protocol}/${resourcePath}`, contentCatalog, Object.assign({}, DEFAULT_RESOURCE_ID, { module: 'm2' }), family, encoding)
            await remoteFetch.complete()
            expect(key2).to.equal(`1.0@c1:m2:${family}$t1/${basename}`)
            const files2 = contentCatalog.getAll()
            expect(files2.length).to.equal(2)
            expect(await getCachedFileNames()).to.equal('no-cache-dir')
            expect(httpRequests.length).to.equal(requestCount)
          })

          it(`should fetch despite caching ${protocol.slice(0, 5)}`, async () => {
            expect(await getCachedFileNames()).to.equal('no-cache-dir')
            await setup(cacheConfig)
            const key = remoteFetch.fetchToContentCatalog(`${protocol}/${resourcePath}`, contentCatalog, DEFAULT_RESOURCE_ID, family, encoding)
            await remoteFetch.complete()
            expect(key).to.equal(`1.0@c1:m1:${family}$t1/${basename}`)
            const files = contentCatalog.getAll()
            expect(files.length).to.equal(1)
            const cachedHashNames = await getCachedFileNames()
            expect(cachedHashNames.length).to.equal(1)
            expect(httpRequests.length).to.equal(requestCount)
            const cachedContentsPath = path.join(CACHE_DIR, CONTENT_CACHE_FOLDER, PROJECT_DIR, cachedHashNames[0], 'contents')
            const stat1 = await fs.promises.lstat(cachedContentsPath)

            await setup(Object.assign({ fetch: true }, cacheConfig))
            const key2 = remoteFetch.fetchToContentCatalog(`${protocol}/${resourcePath}`, contentCatalog, Object.assign({}, DEFAULT_RESOURCE_ID, { module: 'm2' }), family, encoding)
            await remoteFetch.complete()
            expect(key2).to.equal(`1.0@c1:m2:${family}$t1/${basename}`)
            const files2 = contentCatalog.getAll()
            expect(files2.length).to.equal(2)
            expect((await getCachedFileNames()).length).to.equal(1)
            expect(httpRequests.length).to.equal(requestCount * 2)
            const stat2 = await fs.promises.lstat(cachedContentsPath)
            expect(stat1.mtimeMs).to.equal(stat2.mtimeMs)
          })

          it(`No cache option should fetch and not cache ${protocol.slice(0, 5)} `, async () => {
            expect(await getCachedFileNames()).to.equal('no-cache-dir')
            await setup(noCacheConfig)
            const key = remoteFetch.fetchToContentCatalog(`${protocol}/${resourcePath}`, contentCatalog, DEFAULT_RESOURCE_ID, family, encoding)
            await remoteFetch.complete()
            expect(key).to.equal(`1.0@c1:m1:${family}$t1/${basename}`)
            const files = contentCatalog.getAll()
            expect(files.length).to.equal(1)
            expect(await getCachedFileNames()).to.equal('no-cache-dir')
            expect(httpRequests.length).to.equal(requestCount)
          })

          it(`fetchThen exception with ${protocol.slice(0, 5)} should be able to remove from cache`, async () => {
            expect(await getCachedFileNames()).to.equal('no-cache-dir')
            await setup(cacheConfig)
            remoteFetch.fetchThen(`${protocol}/${resourcePath}`, encoding, (info) => {
              const e = new Error('test error')
              e.info = info
              throw e
            })
            try {
              await remoteFetch.complete()
              expect(false, 'should throw error')
            } catch (e) {
            }
            expect((await getCachedFileNames()).length).to.equal(0)
            expect(httpRequests.length).to.equal(requestCount)
            remoteFetch.promises = []

            //To see if it's in the memory cache, try again without an exception.
            remoteFetch.fetchThen(`${protocol}/${resourcePath}`, encoding, (info) => info)
            await remoteFetch.complete()
            expect(httpRequests.length, httpRequests).to.equal(requestCount * 2)
          })

          it(`fetchThen should not fetch after caching with fetch parameter false ${protocol.slice(0, 5)}`, async () => {
            expect(await getCachedFileNames()).to.equal('no-cache-dir')
            await setup(cacheConfig)
            remoteFetch.fetchThen(`${protocol}/${resourcePath}`, encoding, () => {})
            await remoteFetch.complete()
            const cachedHashNames = await getCachedFileNames()
            expect(cachedHashNames.length).to.equal(1)
            expect(httpRequests.length).to.equal(requestCount)
            const cachedContentsPath = path.join(CACHE_DIR, CONTENT_CACHE_FOLDER, PROJECT_DIR, cachedHashNames[0], 'contents')
            const stat1 = await fs.promises.lstat(cachedContentsPath)

            await setup(Object.assign({ fetch: true }, cacheConfig))
            remoteFetch.fetchThen(`${protocol}/${resourcePath}`, encoding, () => {}, false)
            await remoteFetch.complete()
            expect((await getCachedFileNames()).length).to.equal(1)
            expect(httpRequests.length).to.equal(requestCount)
            const stat2 = await fs.promises.lstat(cachedContentsPath)
            expect(stat1.mtimeMs).to.equal(stat2.mtimeMs)
          })
        })
      })

      describe('test with missing file', () => {
        ;[
          ['patha/to/missing.adoc', 'page', 'utf8'],
          ['examples/missing.png', 'image', 'binary'],
        ].forEach(([resourcePath, family, encoding]) => {
          const basename = path.basename(resourcePath)
          it(`should not fetch missing ${resourcePath} over ${protocol.slice(0, 5)} `, async () => {
            expect(await getCachedFileNames()).to.equal('no-cache-dir')
            await setup(cacheConfig)
            const key = remoteFetch.fetchToContentCatalog(`${protocol}/${resourcePath}`, contentCatalog, DEFAULT_RESOURCE_ID, family, encoding)
            const d = await deferExceptions(() => Promise.all(remoteFetch.promises))
            expect(d).to.throw(missingMessage)
            expect(key).to.equal(`1.0@c1:m1:${family}$t1/${basename}`)
            const files = contentCatalog.getAll()
            expect(files.length).to.equal(0)
            expect((await getCachedFileNames()).length).to.equal(0)
            expect(httpRequests.length).to.equal(requestCount)
          })
        })
      })
    })
  })
})
