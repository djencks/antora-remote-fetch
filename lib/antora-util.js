'use strict'

const fs = require('fs')
const { promises: fsp } = fs
const getCacheDir = require('cache-directory')
const expandPath = require('@antora/expand-path-helper')
const ospath = require('path')
// QUESTION should the cache version track the major version of Antora?
const CONTENT_CACHE_FOLDER = 'content/2'

//Copied from content-aggregator.
/**
 * Expands the content cache directory path and ensures it exists.
 *
 * @param {String} preferredCacheDir - The preferred cache directory. If the value is undefined,
 *   the user's cache folder is used.
 * @param {String} startDir - The directory to use in place of a leading '.' segment.
 *
 * @returns {Promise<String>} A promise that resolves to the absolute content cache directory.
 */
function ensureCacheDir (preferredCacheDir, startDir, subfolder) {
  // QUESTION should fallback directory be relative to cwd, playbook dir, or tmpdir?
  const baseCacheDir =
    preferredCacheDir == null
      ? getCacheDir('antora' + (process.env.NODE_ENV === 'test' ? '-test' : '')) || ospath.resolve('.antora/cache')
      : expandPath(preferredCacheDir, '~+', startDir)
  const cacheDir = ospath.join(baseCacheDir, CONTENT_CACHE_FOLDER, subfolder)
  return fsp
    .mkdir(cacheDir, { recursive: true })
    .then(() => cacheDir)
    .catch((err) => {
      err.message = `Failed to create content cache directory: ${cacheDir}; ${err.message}`
      throw err
    })
}

module.exports = {
  ensureCacheDir,
}
