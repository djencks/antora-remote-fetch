'use strict'

const { createHash } = require('crypto')
const fs = require('fs')
const { promises: fsp } = fs
const ospath = require('path')
const { ensureCacheDir } = require('./antora-util')

//native
const http = require('http')
const https = require('https')

const BASENAME_RX = /^.*\/([^/?]*)(\?.*)?$/
const URL_RX = /^(http|https|file):\/\/(.*)$/
const $allPaths = Symbol('$allPaths')

class RemoteFetch {
  constructor (
    {
      cacheInfo,
      fetch,
      allowHttp,
      allowedFilePaths,
    }) {
    this.cacheInfo = cacheInfo
    this.fetch = fetch
    this.allowHttp = allowHttp
    this.allowedFilePaths = allowedFilePaths

    //Map of url to promise.  When the promise resolves the file has been added to the content catalog.
    this.remoteUrls = new Map()
    this.promises = []
    this.pages = new Set()
  }

  async initialize () {
    if (this.cacheInfo) {
      this.cacheDir = await ensureCacheDir(this.cacheInfo.preferredCacheDir,
        this.cacheInfo.startDir,
        this.cacheInfo.projectDir)
    }
  }

  //Call complete() to await all the promises and reset them
  //so this instance can be reused.
  async complete () {
    return Promise.all(this.promises).then((result) => {
      this.promises = []
      return result
    })
  }

  //returns the resource ID for the page.
  fetchToContentCatalog (url, contentCatalog, { version, component, module, dirname }, family, encoding) {
    const match = url.match(BASENAME_RX)
    const relative = match ? `${dirname}${match[1]}` : `${dirname}${url}`
    const id = `${version}@${component}:${module}:${family}$${relative}`
    if (!this.pages.has(id)) {
      this.pages.add(id)
      this.fetchThen(url, encoding, (info) => {
        if (info) {
          const page = Object.assign({}, info, {
            src: {
              family,
              mediaType: info.mediaType,
              component,
              version,
              module,
              relative,
            },
          })
          contentCatalog.addFile(page)
          return page
        }
      }
      )
    }
    return id
  }

  //Fetches and calls the processor on the file.
  //The processor can reject the file causing it to be removed from the cache
  //by throwing an Error with the info attached.
  fetchThen (url, encoding, processor, fetch) {
    this.promises.push(this.fetchPath(url, encoding, fetch)
      .then(processor)
      .catch(async (e) => {
        const info = e.info
        if (info) {
          if (info.cachePath) {
            await this.removeInfo(info.cachePath)
          }
          if (info.path) {
            this.remoteUrls.delete(info.path)
          }
        }
        throw e
      })
    )
  }

  //Fetches a URL's contents from the cache or URL and adds it to the remoteUrls map.
  //remoteUrls values are info objects containng the url, Buffer, and mediaType.
  //Returns {path, contents, mediaType} for use in a File.
  async fetchPath (url, encoding = 'binary', fetch = this.fetch) {
    const result = this.remoteUrls.get(url)
    if (result) {
      return result
    }
    let cachePath
    if (this.cacheDir) {
      const hash = createHash('sha1')
      hash.update(url)
      const cacheKey = hash.digest('hex')
      cachePath = ospath.join(this.cacheDir, cacheKey)
      if (!fetch && await fsp.stat(cachePath).catch(() => false).then((stat) => stat && stat.isDirectory())) {
        const info = await this.readInfo(cachePath)
        if (info) {
          this.remoteUrls.set(url, info)
          return info
        }
      }
    }
    const info = await this.remoteFetch(url, encoding)
    if (info) {
      this.remoteUrls.set(url, info)
      if (cachePath) {
        info.cachePath = cachePath
        await this.writeInfo(cachePath, info)
      }
    }
    return info
  }

  async remoteFetch (url, encoding) {
    const match = url.match(URL_RX)
    if (!match) {
      return
    }
    const info = { path: url }
    const scheme = match[1]
    const path = match[2]
    if (scheme === 'file') {
      for (const allowedPath of this.allowedFilePaths || []) {
        if (allowedPath === $allPaths || path.startsWith(allowedPath)) {
          try {
            info.contents = encoding === 'utf8'
              ? Buffer.from(await fsp.readFile(path, encoding))
              : Buffer.from(await fsp.readFile(path))
            return info
          } catch (e) {
            throw new Error(`Could not read local file ${url}`, e)
          }
        }
      }
      throw new Error(`Path ${path} not allowed`)
    }
    let client
    const options = {}
    if (scheme === 'http') {
      if (!this.allowHttp) {
        throw new Error(`Http protocol not allowed for ${url}`)
      }
      client = http
    } else {
      client = https
    }
    return new Promise((resolve, reject) => {
      client.get(url, options, (response) => {
        const methods = (encoding === 'utf8') ? new Utf8Methods(info, response, resolve) : new BinMethods(info, response, resolve)
        if (response.statusCode === 200) {
          response.on('data', (data) => methods.onData(data))
          response.on('end', () => {
            methods.onEnd()
          })
          response.on('error', (e) => {
            e.message = `${scheme} error: ${e} for url: ${url}`
            reject(e)
          })
        } else if (response.statusCode === 301 ||
          response.statusCode === 302 ||
          response.statusCode === 303 || //this might be a non-GET method we don't handle.
          response.statusCode === 307) {
          resolve(this.remoteFetch(response.headers.location, encoding)
            .then((info) => {
              info.path = url
              return info
            }))
        } else {
          reject(new Error(`Unexpected statusCode ${response.statusCode} for ${url}`))
        }
      }).on('error', (e) => {
        e.message = `${scheme} error: ${e} for url: ${url}`
        reject(e)
      })
    })
  }

  readInfo (cachePath) {
    return Promise.all([
      fsp.readFile(ospath.join(cachePath, 'contents')),
      fsp.readFile(ospath.join(cachePath, 'info')),
    ]).then(([contents, infoJson]) => {
      const info = JSON.parse(infoJson)
      info.contents = contents
      info.cachePath = cachePath
      return info
    }).catch((e) => {
      console.log(`could not read from cachePath ${cachePath}`, e)
      return false
    })
  }

  async writeInfo (cachePath, { path, contents, mediaType }) {
    try {
      const stat = await fsp.stat(cachePath).catch(() => false)
      if (!stat) {
        await fsp.mkdir(cachePath)
      } else if (stat.isFile()) {
        await fsp.unlink(cachePath)
        await fsp.mkdir(cachePath)
      } else {
        const existing = await fsp.readdir(cachePath)
        if (existing.length === 2 && existing.includes('contents') && existing.includes('info')) {
          return
        }
      }
      return Promise.all([
        fsp.writeFile(ospath.join(cachePath, 'contents'), contents),
        fsp.writeFile(ospath.join(cachePath, 'info'), JSON.stringify({ path, mediaType })),
      ])
    } catch (e) {
      console.log(`could not write cache info for ${path} to ${cachePath}`, e)
      await this.removeInfo(cachePath)
    }
  }

  async removeInfo (cachePath) {
    await Promise.all([
      fsp.unlink(ospath.join(cachePath, 'contents')).catch(() => false),
      fsp.unlink(ospath.join(cachePath, 'info')).catch(() => false),
    ])
    await fsp.rmdir(cachePath).catch(() => false)
  }
}

class Utf8Methods {
  constructor (info, res, resolve) {
    res.setEncoding('utf8')
    this.content = ''
    this.info = info
    this.res = res
    this.resolve = resolve
  }

  onData (data) {
    this.content += data
  }

  onEnd () {
    this.info.contents = Buffer.from(this.content)
    this.info.mediaType = this.res.headers['content-type']
    this.resolve(this.info)
  }
}

class BinMethods {
  constructor (info, res, resolve) {
    this.content = []
    this.info = info
    this.res = res
    this.resolve = resolve
  }

  onData (data) {
    this.content.push(data)
  }

  onEnd () {
    this.info.contents = Buffer.concat(this.content)
    this.info.mediaType = this.res.headers['content-type']
    this.resolve(this.info)
  }
}
module.exports = RemoteFetch
module.exports.$allPaths = $allPaths
